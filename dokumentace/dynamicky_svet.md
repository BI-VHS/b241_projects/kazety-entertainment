# Dynamický svět

## Time Loop

Hra se odehrává v úseku 10 minut (12 hodin in-game). Pokud čas vyprší, tak vybouchne bomba a hráč začíná den od začátku se znalostmi z minulých úseků.

## Chování NPCs

Jednotlivé postavy v dané časy přechází na různá místa. Postavy v rámci svých rutin také dělají různé činnosti, které spouští příslušné animace. Hráč pak může s postavami spustit konverzaci.

![obr](./obrazky/factory.png){width=30%}

![obr](./obrazky/party.png){width=30%}

## Pohyb mezi scénami

Pro pohyb mezi scénami slouží zastávky autobusu, se kterými hráč interaguje pomocí myši kliknutím na název dané scény.

## Déšť

Ve hře je implementován vlastní systém bouřky, která náhodně spouští viditelné blesky s hromy. Zvuk deště se utlumí, pokud hráč vstoupí do vnitřních prostorů.

## Hudba

Hra je doplněna o vlastní hudbu, která hráče provází po celou dobu. Na určitých místech hraje navíc jiná hudba, či různé zvukové efekty, a ta výchozí je tudíž utlumena.