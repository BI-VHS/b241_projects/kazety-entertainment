# Dokumentace statického světa

## Herní svět

Město je středně velké moderní město rozdělené do několika sociálně odlišných čtvrtí. Hráč se pohybuje v časové smyčce, která se resetuje po neúspěšném odhalení bombového útoku. Svět kombinuje realistickou architekturu s prvky stylizovaného designu, aby podtrhl atmosféru hry. Každá lokace má specifický vizuální styl a herní funkce, které podporují mechaniky vyšetřování.

## Scény

### Náměstí (The Square)

**Popis lokace:**
Hlavní náměstí, kde začíná hra. Neutrální prostředí. Dominantou je pódium pro starostovu řeč.

**Klíčové prvky:**
- Otevřený prostor s vysokou koncentrací NPC.
- Bomba, kterou je třeba odjistit.

**Pódium - komentář k modelování:**

Pódium je opět obecné, bez širší inspirace. Jedná se o desku na hranolech. Každý vertikální hranol je vyztužen diagonální podpěrou. Na pódiu se nachází dřevěný podstavec s mikrofony, kde bude přednášena řeč. V přední části zdobí podium červený závěs.

![obr](./obrazky/stand.jpg){width=30%}

**Náměstí - komentář k modelování:**

Náměstí je lokace hlavního dějiště příběhu, inspirace na něj je spíše neurčitá, tvoří ho klasické domy s výlohou pro lokální obchody ve spodních patrech a okny ve vyšších, navrch je zdobí červené taškové střechy.

![obr](./obrazky/square.jpg){width=30%}


### Bohatá čtvrť (Rich Block)

**Popis lokace:**
Luxusní rezidenční oblast s velkými domy v britském stylu (kamenné fasády, velké zahrady). Centrem je residence s večírkem, kde se odehrává klíčová událost.

**Klíčové prvky:**
- Dům majitele továrny
- Vícepatrový mansion s několika místnostmi.

**Dům majitele továrny dům - komentář k modelování:**
Pro dům majitele továrny jsem měl snad okamžitě vizi domu, který je vidět v prvních pár vteřinách filmu The Batman z roku 2022. Dům se jmenuje Two Temple Place.

![Two temple palace](./obrazky/richhouse.jpg){width=30%}

Zjistil jsem si, že jeho architektura patří do neo gotického období a tedy jsem si našel podobné budovy. Posléze jsem narazil na obrázek, který jsem použil jako hlavní a prakticky jedinou inspiraci k modelování.

![Neo gotický dům](./obrazky/dum.jpg){width=30%}

Jediné, co jsem převzal z původního Two Temple Place je jeho design kamenného oplocení, které jsem akorát zjednodušil.

![Prvni verze modelu starostova domu](./obrazky/modelrichhouse.png){width=30%}

Model mi ale nevyhovoval, interiér byl takový odfláknutý a bez nápadu, a ty okna až moc symetrický a nevypadalo to v podstatě jako reálný dům, ale spíš jako výplod někoho, kdo hraje the sims. Takže s první verzí jako předlohou jsem začal místo designu exteriéru a podle toho interiér, udělal interiérem a podle toho udělal exteriér.

![alt text](./obrazky/modernDum.png){width=30%}

Nakonec z toho vzniknul dům na který jsem relativně pyšný, kromě zařízení toho domu, to mi furt moc nejde.

![alt text](./obrazky/image.png){width=30%}

![alt text](./obrazky/image-1.png){width=30%}

![alt text](./obrazky/image-2.png){width=30%}

![alt text](./obrazky/image-3.png){width=30%}

![alt text](./obrazky/image-4.png){width=30%}

### Chudá čtvrť a Továrna (Poor Block & Factory)

**Popis lokace:**
Dvě propojené lokace v jedné scéně, oddělené širokou ulicí. Na jedné straně panelákové sídliště, na druhé industriální komplex továrny. Kontrast chudoby a průmyslu podtrhuje sociální napětí ve městě.

**Klíčové prvky:**
- **Chudá čtvrť (severní strana ulice):**
  - Paneláky
  - Jeden přístupný byt v paneláku
- **Továrna (jižní strana ulice):**
  - Průmyslový komplex s vysokými komíny, hroudy štěrku

**Chudá čtvrť - Komentář k modelování (Tomáš Krejčík):**
Pro chudou čtvrť jsem měl v hlavě vizi panelového domu, podobného domům ze hry Shadows of doubt.

![Shadows of doubt inspirace](./obrazky/shadows.png){width=30%}

![Prvni verze modelu chudého paneláku](./obrazky/modelpanelak.png){width=30%}

První verze, stejně jako u starostova domu, se mi opět až tak moc nezamlouvala. Hlavně tedy ten panelák, ten byl v podstatě dočasný "jen ať tam něco je". Bez interiéru, bez ničeho. Nahradil jsem ho, vzhledově né moc lepší, alternativou

![alt text](./obrazky/image-5.png){width=30%}

Ale vzhled dokáže námi vybrané osvětlení scény relativně schovat. (Modrý kruh je něco, něco, nevím co, ale vidět to nebude)

![alt text](./obrazky/image-6.png){width=30%}

Jako u startostova domu jsem apartmán nejdřív zevnitř a pak řešil venek. (zelený kruhy zase nebudou)\

![alt text](./obrazky/image-7.png){width=30%}

![alt text](./obrazky/image-8.png){width=30%}

![alt text](./obrazky/image-9.png){width=30%}

Pro fabriku jsem měl zprvu plán udělat obrovskou halu s kovárnou (možná design změním právě na ni). Ale rozhodl jsem se spíš udělat továrnu, kterou jsem si v hlavě představil jako spojení Velkopopovického pivovaru, proto ty vysoké komíny,

![Pivovar inspirace](./obrazky/pivovar.jpg){width=30%}

a jedné nedaleké fabriky nedaleko od mého domu.

![Prvni verze modelu fabriky](./obrazky/modelfabrika.png){width=30%}

Fabrika mi při revoluci statických scén vzhledově relativně vyhovovala, až teda na velmi viditelně opakované assety v pozadí a proto jsem je nahradil a dodal více různorodých assetů po okolí.

![alt text](./obrazky/image-10.png){width=30%}

![alt text](./obrazky/image-11.png){width=30%}

![alt text](./obrazky/image-12.png){width=30%}

A opět pohled ve hře:

![alt text](./obrazky/image-13.png){width=30%}

### Diner a Most

**Popis lokace:**

Na náplavce pod mostem se nachází restaurace, kde se odehrává část příběhu, dostaneme se do ní po zmíněném schodišti z mostu dolů. Předlohou je standardní americký diner.

**Komentář k modelování (Patrik Cinert):**
Hlavní inspirací byl Palackého most přes řeku Vltavu v Praze. Zapůjčil jsem si vizuální styl různé barvy kamene, tvar pilířů i oblouků. Model má vyšší mostovku, než zmíněná předloha. Pilíře jsou ze dvou objektů, dále mostovka má vlastní objekt a povrch mostu je taktéž tvořen vlastním objektem. Kamenné zábradlí je naškálováno tak, aby se na jeden padesátimetrový segment mostu vešlo 17 kusů zábradlí. Jeden kus je tvořen jedním hlavním sloupem a pěti stylizovanými sloupky. Segment mostu je osově symetrický po X i Y ose, na délku má 50 metrů, na šířku přibližně 13. Každý segment obsahuje na stranách polovinu pilíře, což umožňuje řetězení segmentů pro libovolnou délku mostu.

![Inspirace palackého most](./obrazky/palackeho_most_u.jpg){width=30%}

![most ve scéně](./obrazky/modelmost.png){width=30%}

**Rozšíření mostu v podobě schodiště:**
Z mostu vede železné schodiště, které už nenese reálnou předlohu, jako most samotný. Vstup je vytvořen jedním chybějícím segmentem zábradlí mostu. Horní platforma je uchycena přímo do mostovky, druhá je již upevněna v betonovém ostrově, na něž schodiště sestupuje. Zábradlí je z jednobarevného lesklého materiálu, schody a platformy nesou texturu plechu.

![schodiště z msotu ve scéně](./obrazky/modelmostschodiste.png){width=30%}

**Restaurace pod mostem (Diner):**
Místo setkávání NPC z různých vrstev.

### Výrazné assety (modelování)
Lampy a lavičky se vyskytují ve více scénách, jedná se o stejný asset

**Okrasná lampa**
Inspiraci na zeleně natřenou stylovější pouliční lampu jsem našel zde

![obr](./obrazky/lamp_ref.jpg){width=30%}

a model použitý ve hře:

![obr](./obrazky/lamp.png){width=30%}

**Lavička**

Lavička nemá širší inspiraci, jde o generic lavičku se třemi prkny na sezení, dvěma na opěrku a s železnou kosntrukcí

![obr](./obrazky/lavicka.jpg){width=30%}