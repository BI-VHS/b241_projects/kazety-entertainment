# Komplexní svět

## Dialogový systém

Vlastní plugin pro dialogový systém nám umožňuje použít složité konverzační grafy a navázat konverzace na různé činnosti postav. Ve hře je také možnost zadat klíčová slova pro přístup do skrytých sekcí konverzace. Postavy v rámci konverzace prozradí hráči další klíčová slova (v konverzaci jsou vizuálně odlišena), které mohou být následně využita.

![obr](./obrazky/dynamic/Dialogue1.png){width=30%}

![obr](./obrazky/dynamic/Dialogue2.png){width=30%}

## Sběr důkazů + panel detektiva

Ve hře je implementován systém pro prohlížení objektů, který hráči umožní sběr otisků prstů do detektivova panely, které poslouží k vyřešení záhady. Hráč si může v panelu uchovávat informace které budou užitečné později.

![obr](./obrazky/board.png){width=30%}

## Odjištění bomby

Pokud hráč najde bombu, potřebuje mít správný otisk prstu k jejímu odjištění. Pokud zvolí špatně, bomba v zápětí vybouchne.