# Kazety Entertainment

Přijměte roli detektiva a prozkoumávejte otevřený svět. Mluvte s lidmi, zapisujte si a prozkoumávejte předměty. Zjistěte, co se vlastně chystá, či chystalo, pokud se rohodnete jít proti proudu času.

## Ovládání:

| Akce | Klávesa |
| --- | --- |
| Pohyb | WASD + myš
| Interakce | E
| Výběr destinace na zastávce | Levá myš
| Panel detektiva | Tab
| Reset | Backspace |

## Dokumentace:
[Dokumentace statického světa](./dokumentace/staticky_svet.md)

[Dokumentace dynamického světa](./dokumentace/dynamicky_svet.md)

[Dokumentace komplexního světa](./dokumentace/komplexni_svet.md)